import { NgModule } from '@angular/core';

import { NotFoundComponent } from './pages/not-found.component';
import { Routes, RouterModule } from '@angular/router';

const ROUTES: Routes = [
  { path: '', component: NotFoundComponent}
]

@NgModule({
  imports: [
    RouterModule.forChild(ROUTES)
  ],
  exports: [],
  declarations: [NotFoundComponent],
  providers: [],
})
export class NotFoundModule { }
